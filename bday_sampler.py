import sys, random
from optparse import OptionParser

DISTFILE = 'bdata.txt'

def main():
    parser = OptionParser()
    parser.add_option("-n", "--numsamples",
        action="store", type="int", dest="numsamples", default=10000,
        help="number of sampling trials to run")
    parser.add_option("-l", "--leap",
        action="store_true", dest="leap", default=False,
        help="Treat Feb 29 (leap year) as a distinct date which must be covered. The default is to ignore.")
    parser.add_option("-f", "--numfriends",
        action="store", type="int", dest="numfriends", default=1000,
        help="number of friends in constrained mode")
    parser.add_option("-c", "--constrained",
        action="store_true", dest="constrained", default=False,
        help="run in constrained mode: calculate probility all bdays covered by -f (--numfriends) friends")
    (options, args) = parser.parse_args()

    # read the distribution
    dist = read_dist_file(DISTFILE, options.leap)
    print "Distribution over days:"
    for i in xrange(len(dist)):
        print "Day %d:\t %.5f"%(i+1, dist[i])
    print

    if options.constrained:
        prob = do_constrained_trial(options.numsamples, dist, options.numfriends)

        print "Probability of all bdays covered by %d friends: %.5f"%(options.numfriends, prob)
        print "   from %d trials."%(options.numsamples)

    else:
        avg = do_coverage_trial(options.numsamples, dist)

        print "Average number of people required: %.2f"%(avg)
        print "   from %d trials."%(options.numsamples)

def do_constrained_trial(numsamples, dist, k):
    hits = 0.0
    for i in xrange(numsamples):
        if i > 1 and i % 100 == 1:
            print "Done %d trials..."%(i - 1)
        covered = [False] * len(dist)

        for j in xrange(k):
            sample_person(dist, covered)
            if all(covered):
                break

        if all(covered):
            hits += 1

    return hits / numsamples

def do_coverage_trial(numsamples, dist):
    samples = []
    for i in xrange(numsamples):
        if i > 1 and i % 100 == 1:
            print "Done %d trials..."%(i - 1)
        covered = [False] * len(dist)
        num_people = 0

        while not all(covered):
            #sample a new person
            num_people += 1
            sample_person(dist, covered)

        samples.append(num_people)

    samples = map(float, samples)
    return sum(samples) / len(samples)

def sample_person(dist, covered):
    accum = 0.0
    sample = random.random()
    for i in xrange(len(dist)):
        if sample <= accum + dist[i]:
            covered[i] = True
            return
        else:
            accum += dist[i]
    # should not reach here...
    return False

def read_dist_file(DISTFILE, leap):
    counts = []
    with open(DISTFILE, 'r') as f:
        for l in f.readlines():
            date, count = l.strip().split()
            if (not leap) and date=="0229":
                continue
            count = float(count)
            counts.append(count)

    # Check we have the right number of dates 
    expected = 366 if leap else 365
    assert len(counts) == expected

    total = sum(counts)
    counts = map(lambda x: x / total, counts)

    return counts

if __name__ == "__main__":
    main()
    
